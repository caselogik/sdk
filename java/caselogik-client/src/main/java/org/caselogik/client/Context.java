package org.caselogik.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.caselogik.api.Task;
import org.caselogik.api.WorkData;

import java.io.IOException;
import java.util.Map;

abstract public class Context {

    protected RestGateway restGateway;
    protected ObjectMapper objMapper;


    //----------------------------------------------------------
    public Context(RestGateway restGateway, ObjectMapper objMapper) {
        this.restGateway = restGateway;
        this.objMapper = objMapper;
    }

    //----------------------------------------------------------
    protected String putArg(String url, String token, long value){
        return putArg(url, token, Long.toString(value));
    }

    protected String putArg(String url, String token, String value){
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(token).append("}");
        return url.replace(sb.toString(), value);
    }

    //---------------------------------------------------------
    protected Map<String, Class<? extends WorkData>> workDataClassMap;

    protected void loadWorkData(Task task){
        if(workDataClassMap!=null && workDataClassMap.containsKey(task.workDataType)) {
            try {
                task.workData = (WorkData) objMapper.readValue(task.workDataJson, workDataClassMap.get(task.workDataType));
            } catch (IOException e) {
                throw new RuntimeException("Unable to deserialise JSON of Task(id=" + task.id + ") to instance of " + task.workDataType);
            }
        }
    }
}
