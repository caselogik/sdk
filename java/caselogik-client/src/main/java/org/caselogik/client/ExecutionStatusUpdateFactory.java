package org.caselogik.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.caselogik.api.WorkData;
import org.caselogik.api.commands.task.RecordTaskCompleted;
import org.caselogik.api.commands.task.RecordTaskFailed;

public class ExecutionStatusUpdateFactory {

    private ObjectMapper objMapper;

    //------------------------------------------------------------
    public ExecutionStatusUpdateFactory(ObjectMapper objMapper) {
        this.objMapper = objMapper;
    }

    //--------------------------------------------------------------------
    public RecordTaskCompleted recordTaskCompleted(WorkData workData) {
        RecordTaskCompleted resp = null;
        if(workData!=null){
            SerialisedWorkData swd = SerialisedWorkData.from(workData, objMapper);
            resp = new RecordTaskCompleted(swd.json, swd.type);
        }
        else resp = new RecordTaskCompleted(null, null);
        return resp;
    }

    //--------------------------------------------------------------------
    public RecordTaskCompleted recordTaskCompleted() {
        return recordTaskCompleted(null);
    }

    //--------------------------------------------------------------------
    public RecordTaskFailed recordTaskFailed(String reason, WorkData workData) {
        RecordTaskFailed resp = null;
        if(workData!=null){
            SerialisedWorkData swd = SerialisedWorkData.from(workData, objMapper);
            resp = new RecordTaskFailed(reason, swd.json);
        }
        else resp = new RecordTaskFailed(reason, null);
        return resp;
    }

    //--------------------------------------------------------------------
    public RecordTaskFailed recordTaskFailed(String reason) {
        return recordTaskFailed(reason);
    }
}
