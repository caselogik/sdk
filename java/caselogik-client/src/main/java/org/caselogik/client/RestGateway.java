package org.caselogik.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class RestGateway {

    private HttpClient httpClient;
    private String serviceUrl;
    private ObjectMapper mapper = new ObjectMapper();
    private ResponseHandler<String> handler = new BasicResponseHandler();

    //-------------------------------------
    public RestGateway(String serviceUrl) {
        this.serviceUrl = serviceUrl;
        httpClient = HttpClientBuilder.create().build();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    //----------------------------------------------------
    public <I, O> O sendCommand(I cmd, String url, Class<O> clazz){
        HttpPut request = new HttpPut(serviceUrl+url);
        O resp = null;
        try {
            StringEntity entity = new StringEntity(mapper.writeValueAsString(cmd), ContentType.APPLICATION_JSON);
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            String body = handler.handleResponse(response);
            int code = response.getStatusLine().getStatusCode();


            if(code == 200) {
                resp = mapper.readValue(body, clazz);
            }

        }
        catch(HttpResponseException httpEx){
            throw new RuntimeException("REST call commandFailed for request "+request, httpEx);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }


    //----------------------------------------------------
    public <O> List<O> list(String url, Class<O[]> clazz, Map<String, String> queryParams){

        HttpGet request = null;

        try {
            URIBuilder builder = new URIBuilder(serviceUrl+url);
            if(queryParams!=null){
                for(String k : queryParams.keySet()){
                    builder.setParameter(k, queryParams.get(k));
                }
            }
            request = new HttpGet(builder.build());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

       List<O> resp = null;
        try {
            HttpResponse response = httpClient.execute(request);
            int code = response.getStatusLine().getStatusCode();


            if(code == 200) {
                String body = handler.handleResponse(response);
                resp = Arrays.asList(mapper.readValue(body, clazz));
            }
            else if(code == 500){
                String message = EntityUtils.toString(response.getEntity(), "UTF-8");
                throw new RuntimeException("REST call commandFailed for request "+request +
                        "\nHttp Status: " + code +
                        "\nReason - "+message);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    //-------------------------------------------------------
    public <O> Optional<O> get(String url, Class<O> clazz){

        HttpGet request = null;

        try {
            URIBuilder builder = new URIBuilder(serviceUrl+url);
            request = new HttpGet(builder.build());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        O resp = null;
        try {
            HttpResponse response = httpClient.execute(request);
            int code = response.getStatusLine().getStatusCode();


            if(code == 200) {
                String body = handler.handleResponse(response);
                resp = mapper.readValue(body, clazz);
            }
            else if(code == 500){
                String message = EntityUtils.toString(response.getEntity(), "UTF-8");
                throw new RuntimeException("REST call commandFailed for request "+request +
                        "\nHttp Status: " + code +
                        "\nReason - "+message);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(resp);
    }

}
