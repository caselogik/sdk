package org.caselogik.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.caselogik.api.Task;
import org.caselogik.api.Worker;
import org.caselogik.api.commands.task.ClaimTaskInQueue;
import org.caselogik.api.commands.task.TaskCmdResponse;

import java.util.List;

public class WorkListContext extends Context {

    private Worker worker;

    //----------------------------------------------------------
    public WorkListContext(String workerId, RestGateway restGateway, ObjectMapper objMapper) {
        super(restGateway, objMapper);
        this.worker = Worker.human(workerId);
    }

    //-------------------------------------
    public List<Task> getTasks() {
        String url = putArg(Const.Urls.WORKLISTS_, "workerId", worker.id );
        List<Task> tasks = restGateway.list(url, Task[].class, null);
        tasks.forEach(t -> loadWorkData(t));
        return tasks;
    }

    //--------------------------------------------------
    public TaskCmdResponse claimTaskInQueue(String processId, String taskNodeId) {
        ClaimTaskInQueue cmd = new ClaimTaskInQueue(worker);
        String url = putArg(putArg(Const.Urls.CLAIM_TASK_, "processId", processId), "taskNodeId", taskNodeId);
        TaskCmdResponse resp = restGateway.sendCommand(cmd, url, TaskCmdResponse.class);
        return resp;
    }
}
