package org.caselogik.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.caselogik.api.Case;
import org.caselogik.api.TaskQueue;
import org.caselogik.api.WorkItem;
import org.caselogik.api.commands.wfcase.CaseCmdResponse;
import org.caselogik.api.commands.wfcase.CreateCase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessContext extends Context {

    private String processId;

    //----------------------------------------------------------
    public ProcessContext(String processId, RestGateway restGateway, ObjectMapper objMapper) {
        super(restGateway, objMapper);
        this.processId = processId;
    }

    //----------------------------------------------------------
    public CaseCmdResponse createCase(WorkItem workItem){
        SerialisedWorkData swd = SerialisedWorkData.from(workItem, objMapper);
        CreateCase cmd = new CreateCase(processId, swd.json, swd.type,-1L);
        CaseCmdResponse resp =  restGateway.sendCommand(cmd, Const.Urls.CASES, CaseCmdResponse.class);
        return resp;
    }

    //--------------------------------------------------
    public List<TaskQueue> getTaskQueues() {
        String url = putArg(Const.Urls.TASK_QUEUES_, "processId", processId );
        List<TaskQueue> taskQueues =  restGateway.list(url, TaskQueue[].class, null);
        return taskQueues;
    }

    //----------------------------------------------------
    public List<Case> getCases() {
        return internalGetCases(false);
    }

    //----------------------------------------------------
    public List<Case> getLiveCases() {
        return internalGetCases(true);
    }

    //----------------------------------------------------
    private List<Case> internalGetCases(boolean liveOnly) {
        Map<String, String> queryParams = new HashMap<>();
        if(liveOnly) queryParams.put("live", "true");
        queryParams.put("processId", processId);
        List<Case> cases =  restGateway.list(Const.Urls.CASES, Case[].class, queryParams);
        return cases;
    }
}
