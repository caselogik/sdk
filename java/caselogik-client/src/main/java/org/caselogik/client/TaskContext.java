package org.caselogik.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.caselogik.api.Task;
import org.caselogik.api.WorkData;
import org.caselogik.api.Worker;
import org.caselogik.api.commands.task.*;

import java.util.Optional;

public class TaskContext extends Context {

    private long taskId;

    public TaskContext(long taskId,
                       RestGateway restGateway,
                       ObjectMapper objMapper) {
        super(restGateway, objMapper);
        this.taskId = taskId;
        executionStatusUpdateFactory = new ExecutionStatusUpdateFactory(objMapper);
    }

    //--------------------------------------
    public Task get() {

        String url = putArg(Const.Urls.TASK_, "taskId", taskId);
        Optional<Task> taskOpt = restGateway.get(url, Task.class);
        if (taskOpt.isPresent()) {
            Task task = taskOpt.get();
            loadWorkData(task);
            return task;
        } else throw new RuntimeException("Task with id=" + taskId + " not found");
    }

    //------------------------------------
    public void allocate(Worker worker) {
        AllocateTask cmd = new AllocateTask(worker);
        String url = putArg(Const.Urls.ALLOCATE_TASK_, "taskId", taskId);
        TaskCmdResponse resp = restGateway.sendCommand(cmd, url, TaskCmdResponse.class);
        checkResponse(resp);
    }

    //------------------------------------
    public void activate() {
        ActivateTask cmd = new ActivateTask();
        String url = putArg(Const.Urls.ACTIVATE_TASK_, "taskId", taskId);
        TaskCmdResponse resp = restGateway.sendCommand(cmd, url, TaskCmdResponse.class);
        checkResponse(resp);
    }

    //---------------------------------------------------
    public void recordCompleted(WorkData workData) {
        RecordTaskCompleted cmd = executionStatusUpdateFactory.recordTaskCompleted(workData);
        String url = putArg(Const.Urls.RECORD_TASK_COMPLETED_, "taskId", taskId);
        TaskCmdResponse resp = restGateway.sendCommand(cmd, url, TaskCmdResponse.class);
        checkResponse(resp);
    }
    //---------------------------------------------------
    public void recordCompleted() {
        recordCompleted(null);
    }

    //--------------------------------------------------------------------
    private ExecutionStatusUpdateFactory executionStatusUpdateFactory;

    public ExecutionStatusUpdateFactory newExecutionStatusUpdate(){
        return executionStatusUpdateFactory;
    }

    //--------------------------------------------------------------------
    public RecordTaskFailed newTaskFailedResponse(String reason, WorkData workData) {
        return workData!=null?
                new RecordTaskFailed(reason, SerialisedWorkData.from(workData, objMapper).json) :
                new RecordTaskFailed(reason, null);
    }

    //--------------------------------------------------------------------
    public RecordTaskFailed createTaskFailedResponse(String reason) {
        return newTaskFailedResponse(reason, null);
    }

    //--------------------------------------------------------------------
    public RecordTaskProgress newTaskProgressResponse(WorkData workData) {
        return workData!=null?
                new RecordTaskProgress(SerialisedWorkData.from(workData, objMapper).json) :
                new RecordTaskProgress(null);
    }

    //--------------------------------------------------------------------
    public RecordTaskProgress createTaskProgressResponse() {
        return newTaskProgressResponse(null);
    }


    //---------------------------------------------------
    private void checkResponse(TaskCmdResponse response){
        if(!response.message.equalsIgnoreCase("OK")){
            throw new CLException(response.message);
        }
    }
}