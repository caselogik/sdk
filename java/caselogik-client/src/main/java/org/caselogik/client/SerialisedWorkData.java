package org.caselogik.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.caselogik.api.WorkData;

//Internal class
class SerialisedWorkData {

    public String json;
    public String type;

    //------------------------------------------------------------------------------
    public static SerialisedWorkData from(WorkData workData, ObjectMapper objMapper){
        SerialisedWorkData result = null;
        if (workData != null) {
            result = new SerialisedWorkData();
            try {
                result.json = objMapper.writeValueAsString(workData);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("Failed to serialize workdata", e);
            }
            result.type = workData.getClass().getName();
        }
        return result;
    }
}
