package org.caselogik.client;

class Const {

    class Urls {

        //Convention: Trailing _ in name means has path parameter token that needs to be replaced
        public static final String CASES = "/cases";
        public static final String PROCESSES = "/processes";

        public static final String TASK_ = "/tasks/{taskId}";
        public static final String TASK_QUEUES_ = "/task-queues/{processId}";

        public static final String WORKLISTS_ = "/worklists/{workerId}/tasks";
        public static final String ALLOCATE_TASK_ = TASK_+"/allocate";
        public static final String ACTIVATE_TASK_ = TASK_+"/activate";
        public static final String RECORD_TASK_COMPLETED_ = TASK_+"/record-completed";
        public static final String CLAIM_TASK_ = TASK_QUEUES_+"/{taskNodeId}/claim";
    }

}