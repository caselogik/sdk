package org.caselogik.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.caselogik.api.WfProcess;
import org.caselogik.api.WorkData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Client {

    private RestGateway restGateway;
    private ObjectMapper objMapper;
    private Map<String, Class<? extends WorkData>> workDataClassMap;

    //--------------------------------------
    public Client(String url){
        restGateway = new RestGateway(url);
        objMapper = new ObjectMapper();
    }

    //--------------------------------------
    public TaskContext task(long id){
        TaskContext tc = new TaskContext(id, restGateway, objMapper);
        tc.workDataClassMap = workDataClassMap;
        return tc;
    }

    //--------------------------------------
    public ProcessContext process(String id){
        return new ProcessContext(id, restGateway, objMapper);
    }

    //--------------------------------------
    public WorkListContext workList(String workerId){
        WorkListContext wc = new WorkListContext(workerId, restGateway, objMapper);
        wc.workDataClassMap = workDataClassMap;
        return wc;
    }

    //-------------------------------------
    public List<WfProcess> getAllProcesses() {
        List<WfProcess> procs =  restGateway.list(Const.Urls.PROCESSES, WfProcess[].class, null);
        return procs;
    }

    //-----------------------------------------------------------
    public void enableWorkDataDeserialisationFor(Class<? extends WorkData> ... workDataClasses){
        workDataClassMap = new HashMap<>();
        for(Class<? extends WorkData> clazz : workDataClasses){
            workDataClassMap.put(clazz.getName(), clazz);
        }
    }
}
