package org.caselogik.client;

public class CLException extends RuntimeException {

    public CLException(String message) {
        super(message);
    }
}
